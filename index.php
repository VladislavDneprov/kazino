<?php 
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage g-r
 */
get_header(); ?>


<div class="background">
	<div class="wide-container">
		<div class="content row">
			<!-- sidebar left -->
			<aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php get_sidebar('left'); ?>
			</aside>
			<!-- /sidebar left -->
			<!-- home text -->
			<section class="description col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<?php get_template_part('part/home/description'); ?>
			</section>
			<!-- /home text -->

			<!-- list games -->
			<section class="games col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<?php get_template_part('part/home/list-games'); ?>
			</section>
			<!-- /list games -->
		</div>
	</div>
</div>
<?php get_footer(); ?>