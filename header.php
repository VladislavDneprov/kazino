<?php
/**
 * wp_nav_menu(array('theme_location' => 'languages', 'container_class' => 'lang dropdown','container' => 'div')) - Top menu
 */
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title>

        <?php wp_head(); ?>
    </head>
    
    <body>
        <header id="top">
            <div class="wide-container">
                <!--<nav class="menu">
                    <ul>
                        <li><a href="">Игровые автоматы</a></li>
                        <li><a href="">Игровые автоматы</a></li>
                        <li><a href="">Игровые автоматы</a></li>
                        <li><a href="">Игровые автоматы</a></li>
                    </ul>
                </nav>-->
                <?php wp_nav_menu(array('theme_location' => 'top', 'container_class' => 'menu','container' => 'nav')); ?>
            </div>
        </header>    