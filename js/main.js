jQuery(function($){

	function Menu(){
		var top = $(document).scrollTop();
		if (top > 800) {
			$("#to-top").show();							
		}				
		else {
			$("#to-top").hide();						
		}
	}

	$(document).ready(function(){

		Menu();

		$(window).load(function(){
			$('.back-full').remove();
		});

		$('#to-top').click(function(){
	        var el = $(this).attr('href');
	        $('body').animate({
	            scrollTop: $(el).offset().top}, 700);
	        return false; 
		});
		
		$(window).scroll(function(){
			Menu();
		});
		
	});
});