<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage money-online
 *
 * wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'col-lg-4 col-md-4 col-sm-4 col-xs-12','container' => ''))
 */

?>
		 <footer>		 	
		 	<?php wp_footer(); ?>
		 	<div class="wide-container">
                <?php wp_nav_menu(array('theme_location' => 'footer', 'container_class' => 'menu','container' => 'nav')); ?>
                <p class="text-center">Казино 1© 2014-<?= date('Y'); ?> Все игровые автоматы на нашем сайте доступны бесплатно и без регистрации.</p>
            </div>
            <a href="#top" id="to-top"><img src="<?= get_template_directory_uri().'/img/top.png'; ?>"></a>
		 </footer>
		 <div class="back-full">
			<div>
				<i class="fa fa-spinner fa-spin fa-4x"></i> 
			</div>
		</div>
	</body>
		
	<?php get_template_part('part/modal-windows'); ?>
</html>