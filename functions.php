<?php
/**
 * Author: Vlad Dneprov, https://www.linkedin.com/in/vladislav-dneprov
 * Author URI: http://jaguar-team.com
 * Date: 11.11.2016
 * Version: 1.0.0
 */

// load_theme_textdomain( 'gr', get_template_directory() . '/languages' );

require_once ( __DIR__.'/core/custom-post.php' );
require_once ( __DIR__.'/core/breadcrumbs.php' );
require_once ( __DIR__.'/core/widget.php' );
require_once ( __DIR__.'/core/user-functions.php' );

/** add javascript **/
add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', 		get_template_directory_uri().'/js/lib/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'slick', 			get_template_directory_uri().'/js/lib/slick/slick.min.js', array( 'jquery' ), '', true );
	//wp_enqueue_script( 'masked', 			get_template_directory_uri().'/js/lib/jquery.inputmask.bundle.min.js', array( 'jquery' ), '', true );
    wp_enqueue_media();
	/** custom **/
	wp_enqueue_script( 'main', 				get_template_directory_uri().'/js/main.js', array( 'jquery' ), '', true );

}

/** add css **/
add_action( 'wp_print_styles', 'add_styles' );
function add_styles() {

	wp_localize_script('main', 'myajax',
		array(
			'url' => admin_url('admin-ajax.php')
		)
	);

	/** lib **/
	wp_enqueue_style( 'bootstrap', 			get_template_directory_uri().'/css/lib/bootstrap.min.css' );
	wp_enqueue_style( 'slick', 				get_template_directory_uri().'/js/lib/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 		get_template_directory_uri().'/js/lib/slick/slick-theme.css' );

	/** fonts **/
	wp_enqueue_style( 'font-awesome', 		get_template_directory_uri().'/css/lib/font-awesome.min.css' );

	/** custom **/
	wp_enqueue_style( 'main', 				get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'global', 			get_template_directory_uri().'/css/global.css' );
    wp_enqueue_style( 'global-tablet', 		get_template_directory_uri().'/css/global-tablet.css' );
    wp_enqueue_style( 'global-mobile', 		get_template_directory_uri().'/css/global-mobile.css' );

	if ( is_home() ) {
        wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
        wp_enqueue_style( 'index-tablet', 	get_template_directory_uri().'/css/index-tablet.css' );
        wp_enqueue_style( 'index-mobile', 	get_template_directory_uri().'/css/index-mobile.css' );
    }

	if ( is_single() ) {
		wp_enqueue_style( 'single', 		get_template_directory_uri().'/css/single.css' );
        wp_enqueue_style( 'single-tablet', 	get_template_directory_uri().'/css/single.css' );
        wp_enqueue_style( 'single-mobile', 	get_template_directory_uri().'/css/single.css' );
	}

    if ( is_singular( 'games' ) ) {
        wp_enqueue_style( 'single-games', 			get_template_directory_uri().'/css/single-games.css' );
        wp_enqueue_style( 'single-games-tablet', 	get_template_directory_uri().'/css/single-games-tablet.css' );
        wp_enqueue_style( 'single-games-mobile', 	get_template_directory_uri().'/css/single-games-mobile.css' );
    }
    if ( is_tax( 'game-rooms' ) ) {
    	wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
    	wp_enqueue_style( 'single-games', 	get_template_directory_uri().'/css/single-games.css' );
    }
}

add_theme_support( 'post-thumbnails' ); # add thubnails

remove_filter( 'the_content', 'wpautop' );

/** registration nav-menu **/
register_nav_menus(array(
	'top' 			=> 'Main menu',
	'footer' 		=> 'Footer',
));


add_action('wp_ajax_list_games', 'my_action_callback');
add_action('wp_ajax_nopriv_list_games', 'my_action_callback');
function my_action_callback() {
	
	if (!isset($_POST['offset']))
		wp_die();

	$offset = $_POST['offset'];

	$posts = get_posts( array(
		'numberposts'     => 15,
		'orderby'         => 'meta_value_num',
		'offset'          => $offset,
		'post_type'       => 'games',
		'meta_key'        => 'views',
		'post_status'     => 'publish'
	) );

	$result = array();

	foreach ($posts as $number => $post) {
		$result[$number]['title'] = $post->post_title;
		$result[$number]['img'] = get_the_post_thumbnail_url($post->ID);
		$result[$number]['url'] = get_permalink($post->ID);
	}

	wp_send_json($result);

	// выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
	wp_die();
}