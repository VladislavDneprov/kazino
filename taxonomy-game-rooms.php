<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 30.11.2016 17:18
 * Version: 1.0.0
 */
get_header();?>


<div class="background">
    <div class="wide-container">
        <div class="content row">
            
            <section class="games col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="single">
                    <?php if ( have_posts() ) : ?>

                        <?php
                            $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                            $cat_data = get_option("category_" . $term->term_id);
                        ?>
                        <div class="title">
                            <?= single_cat_title( '', false ); // name category ?>                 
                        </div>
                        <div class="img" style="height: 450px;">
                            <img src="<?= $cat_data["cat_title"]; // img catefory url ?>">
                        </div>
                        <div class="text" style="margin: 30px 0;">
                            <?= term_description(); // desc category ?>              
                         </div> 
                </div>
                           
                        
                        
                    <div class="games-container">
                        <?php while ( have_posts() ) : the_post(); ?>
                            <div class="block">
                                <a href="<?= get_permalink(get_the_id());?>">
                                    <div class="img">

                                        <?= get_the_post_thumbnail(get_the_id(), 'thubnail');?>

                                    </div>
                                    <div class="title">
                                        <?php the_title();?>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <?php else : ?>

                        <p>Игр не найдено</p>

                    <?php endif; ?>
                
            </section>
            <!-- /list games -->
        </div>
    </div>
</div>
<?php get_footer(); ?>