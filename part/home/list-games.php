<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 30.11.2016 11:54
 * Version: 1.0.0
 */
?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		var offset = 0;
		function setData(offset){
			return { 
					action: 'list_games',
					offset: offset 
				}
		}
		function appendBlock(data){
			for (var i = 0; i < data.length; i++) {
				$('.games-container').append(`<div class="block"><a href="` + data[i].url + `"><div class="img"><img src="` + data[i].img + `"></div><div class="title">` + data[i].title +`</div></a></div>`);
			}

		};
		function goToByScroll(el){
		    $('html,body').animate({
		        scrollTop: el.offset().top - 100},
		        'slow');
		}

		function getData(){
			$.post( myajax.url, setData(offset), function(response) {
				$('.back').hide();
				data = response;
				appendBlock(data);
				offset += 15;				

				$.post( myajax.url, setData(offset), function(resp) {
					if(resp[0]){
						$('.more').show();
					}
					else{
						$('.more').hide();
					}
				});
			});
		}
		getData();
		$('.more').click(function(){
			$('.back').show();		
			getData();
			goToByScroll($('.games-container').children(':nth-child(15)').last());
		});
	});
</script>
	<div class="back">
			<div>
				<i class="fa fa-spinner fa-spin fa-4x"></i> 
			</div>
		</div>
	<div class="games-container">
		
	</div>
	<button class="btn more">Ещё игры</button>
