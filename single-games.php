<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 30.11.2016 13:57
 * Version: 1.0.0
 */

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php setPostViews(get_the_ID()); ?>

	<div class="background">
		<div class="wide-container">
			<div class="content row">
				<!-- sidebar left -->
				<aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<?php get_sidebar('left'); ?>
				</aside>
				<!-- /sidebar left -->
				<!-- home text -->
				<section class="single col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<div class="title">
						<?php the_title(); ?>
					</div>
					<div class="img">
						<iframe style="width: 100%; height:100%;" src="<?= get_post_meta(get_the_id(), 'iframe', 1); ?>"></iframe>
					</div>
					<a href="<?= get_post_meta(get_the_id(), 'url', 1); ?>" title="Играть на деньги" class="btn">Играть на деньги</a>
					<div class="text">
						<?php the_content(); ?>
					</div>
				</section>
				<!-- /home text -->

			</div>
		</div>
	</div>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>