<?php

function register_menu() {
	add_menu_page(
		__( 'Settings theme' ),
		__( 'Settings theme' ),
		'edit_published_posts',
		'setting_theme_main',
		'general_setting_display'
	);
}
add_action('admin_menu', 'register_menu');

function general_setting_display()
{
	?> <div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'general-settings' );
				do_settings_sections( 'general-section' );
				submit_button();
			?>
		</form>
	</div>
	<?php
}

function general_settings_sections(){
	/** registration input name */
	register_setting( 'general-settings', 'general_settings' );

	/** registration sections */
	add_settings_section( 'general_section', __( 'General' ), '', 'general-section' );
	add_settings_section( 'general_section_contact', __( 'Contacts info' ), '', 'general-section' );

	/** registration fields */
	add_settings_field(
		'quantity_investing_companies',
		__( 'On home page display not more than' ),
		'add_field',
		'general-section',
		'general_section',
		array(
			'option' 			=> 	'general_settings',
			'name'				=>	'quantity_investing_companies',
			'type'				=>	'number',
			'placeholder'		=>	6,
			'description'		=>	__( 'Number of investing companies which are displaying on home page' ),
			'label_input'		=>	__( 'investing companies' ),
			'class'				=>	'small-text'
		)
	);

	add_settings_field(
		'quantity_advantages',
		__( 'On home page display not more than' ),
		'add_field',
		'general-section',
		'general_section',
		array(
			'option' 			=> 	'general_settings',
			'name'				=>	'quantity_advantages',
			'type'				=>	'number',
			'placeholder'		=>	6,
			'description'		=>	__( 'Number of advantages which are displaying on home page' ),
			'label_input'		=>	__( 'advantages' ),
			'class'				=>	'small-text'
		)
	);

	add_settings_field(
		'phone',
		__( 'Phone number' ),
		'add_field',
		'general-section',
		'general_section_contact',
		array(
			'option' 			=> 	'general_settings',
			'name'				=>	'phone',
			'type'				=>	'text',
			'placeholder'		=>	__( 'Your phone number' ),
			'description'		=>	__( 'Please, enter your phone number' ).'<span class="dashicons dashicons-phone"></span>',
			'class'				=>	'regular-text qtranxs-translatable'
		)
	);

	add_settings_field(
		'email',
		__( 'E-mail' ),
		'add_field',
		'general-section',
		'general_section_contact',
		array(
			'option' 			=> 	'general_settings',
			'name'				=>	'email',
			'type'				=>	'email',
			'placeholder'		=>	__( 'Your email address' ),
			'description'		=>	__( 'Please, enter your email address' ),
			'class'				=>	'regular-text qtranxs-translatable'
		)
	);

	add_settings_field(
		'skype',
		__( 'Skype' ),
		'add_field',
		'general-section',
		'general_section_contact',
		array(
			'option' 			=> 	'general_settings',
			'name'				=>	'skype',
			'type'				=>	'text',
			'placeholder'		=>	__( 'Your skype login' ),
			'description'		=>	__( 'Please, enter your skype login' ),
			'class'				=>	'regular-text qtranxs-translatable'
		)
	);
}
add_action('admin_init', 'general_settings_sections');

function add_field( $argc ) {
	$value = get_option( $argc['option'] );
	$value = $value[ $argc['name'] ];

	$class = $argc['class'];
	$placeholder = $argc['placeholder'];
	$description = $argc['description'];
	$label_input = ( isset( $argc['label_input'] ) ? $argc['label_input'] : '' );

	echo '
		<input class="'.$class.'" type="'.$argc['type'].'" name="'.$argc['option'].'['.$argc['name'].']" placeholder="'.$placeholder.'" value="'.esc_attr( $value ).'" /> '.$label_input.'
		<p class="description">'.$description.'</p>
	';
}