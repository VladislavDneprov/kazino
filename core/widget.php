<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 30.11.2016 14:32
 * Version: 1.0.0
 */


if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Сайдбар',
        'id'   => 'sidebar',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>'
    ));
}

class FavoriteGames extends WP_Widget {

    function __construct() {
        // Instantiate the parent object
        parent::__construct( false, 'Популярные игры' );
    }

    function widget( $args, $instance ) {
        $posts = get_posts( array(
            'numberposts'     => $instance['count'],
            'orderby'         => 'meta_value_num',
            //'order'           => 'DESC',
            'post_type'       => 'games',
            'meta_key'        => 'views',
            'post_status'     => 'publish'
        ) );

        echo '<section class="popular">';
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
        }
        foreach ($posts as $key => $post)
            echo '
                <div class="block">
                    <a href="'.get_permalink( $post->ID ).'" title="'.$post->post_title.'">
                        <div class="img">'.get_the_post_thumbnail( $post->ID, 'thumbnail' ).'</div>
                        <div class="name">'.$post->post_title.'</div>
                    </a>
                </div>
            ';
        echo '</section>';
    }

    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';

        return $instance;
    }

    function form( $instance ) {
        $title = @ $instance['title'] ?: 'Популярные игры';
        $count = @ $instance['count'] ?: 3;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Название блока</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'count' ); ?>">Количество игр:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="text" value="<?php echo esc_attr( $count ); ?>">
        </p>
        <?php
    }
}

class GameRooms extends WP_Widget {

    function __construct() {
        // Instantiate the parent object
        parent::__construct( false, 'Игровые залы' );
    }

    function widget( $args, $instance ) {
        $terms = get_terms(array(
            'taxonomy' => 'game-rooms',
            'order'         => 'DESC',
            'hide_empty' => false,
            'number'    => $instance['count']
        ));
        echo '<section class="malls">';
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
        }
        foreach ($terms as $key => $post) {
            $cat_data = get_option("category_" . $post->term_id);
            echo '
                <div class="block">
                    <a href="' . get_term_link($post->term_id) . '" class="img" title="' . $post->post_title . '">
                        <img src="' . $cat_data['cat_title'] . '" />
                    </a>
                </div>
            ';
        }
        echo '</section>';
    }

    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';

        return $instance;
    }

    function form( $instance ) {
        $title = @ $instance['title'] ?: 'Игровые залы';
        $count = @ $instance['count'] ?: 5;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Название блока</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'count' ); ?>">Количество игр:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="text" value="<?php echo esc_attr( $count ); ?>">
        </p>
        <?php
    }
}

class TopFive extends WP_Widget {

    function __construct() {
        // Instantiate the parent object
        parent::__construct( false, 'ТОП-5 казино' );
    }

    function widget( $args, $instance ) {
        $terms = wp_get_nav_menu_items($instance['menu']);
        echo '<section class="casino">';
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
        }
        echo '<ul>';
        foreach ($terms as $key => $post) {
            echo '
                <li>
                    <a href="' . $post->url . '" class="img" title="' . $post->title . '">
                        ' . $post->title . '<div class="label">TOP '.($key+1).'</div>
                    </a>
                </li>
            ';
        }
        echo '</ul>';
        echo '<a href="'.$instance['url'].'" class="btn">Играть на деньги</a>';
        echo '</section>';
    }

    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['menu'] = ( ! empty( $new_instance['menu'] ) ) ? strip_tags( $new_instance['menu'] ) : '';
        $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';

        return $instance;
    }

    function form( $instance ) {
        $menus = wp_get_nav_menus();
        $title = @ $instance['title'] ?: 'ТОП-5 казино';
        $url = @ $instance['url'] ?: '#';
        $menu_get = @ $instance['menu'];
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Название блока:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'url' ); ?>">Ссылка на "Играть на деньги":</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'menu' ); ?>">Меню для вывода:</label>
            <select id="<?php echo $this->get_field_id( 'menu' ); ?>" name="<?php echo $this->get_field_name( 'menu' ); ?>">
                <?php foreach ($menus as $number => $menu): ?>
                    <option value="<?= $menu->term_id; ?>" <?php selected($menu_get, $menu->term_id); ?> ><?= $menu->name; ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <?php
    }
}

class BannerTop extends WP_Widget {

    function __construct() {
        // Instantiate the parent object
        parent::__construct( false, 'Баннер' );
    }

    function widget( $args, $instance ) {
        echo '<section class="top">';
        echo '
            <div class="background img">
			    <img src="'.$instance['img'].'">
		    </div>
		    <a href="'.$instance['url'].'" class="btn">Играть на деньги</a>';
        echo '</section>';
    }

    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['menu'] = ( ! empty( $new_instance['menu'] ) ) ? strip_tags( $new_instance['menu'] ) : '';
        $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
        $instance['img'] = ( ! empty( $new_instance['img'] ) ) ? strip_tags( $new_instance['img'] ) : '';

        return $instance;
    }

    function form( $instance ) {
        wp_enqueue_media();
        $url = @ $instance['url'] ?: '#';
        $img = @ $instance['img'] ?: '#';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'url' ); ?>">Ссылка на "Играть на деньги":</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'img' ); ?>">Изображение баннера: <button class="upload_cat_img">Загрузить</button> <img width="80px" src="<?= $img ?>" /></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'img' ); ?>" name="<?php echo $this->get_field_name( 'img' ); ?>" type="hidden" value="<?php echo esc_attr( $img ); ?>">
        </p>
        <script>
            jQuery(document).ready(function(){

                jQuery('.upload_cat_img').click(function(e) {
                    e.preventDefault();
                    var image = wp.media({
                        title: 'Загрузить изображение',
                        // mutiple: true if you want to upload multiple files at once
                        multiple: false
                    }).open()
                        .on('select', function(e){
                            // This will return the selected image from the Media Uploader, the result is an object
                            var uploaded_image = image.state().get('selection').first();
                            // We convert uploaded_image to a JSON object to make accessing it easier
                            // Output to the console uploaded_image
                            //console.log(uploaded_image);
                            var image_url = uploaded_image.toJSON().url;
                            // Let's assign the url value to the input field
                            jQuery('#<?php echo $this->get_field_id( 'img' ); ?>').val(image_url);
                        });
                });
            });
        </script>
        <?php
    }
}

function myplugin_register_widgets() {
    register_widget( 'FavoriteGames' );
    register_widget( 'GameRooms' );
    register_widget( 'TopFive' );
    register_widget( 'BannerTop' );
}
add_action( 'widgets_init', 'myplugin_register_widgets' );