<?php
/*
 * Desc: Register a post type.
 * Name: games
 */
function games_init() {
    $labels = array(
        'name'                  => 'Игры',
        'singular_name'         => 'Игра',
        'menu_name'             => 'Игры',
        'name_admin_bar'        => 'Игра',
        'add_new'               => 'Добавить игру',
        'add_new_item'          => 'Добавить новую игру',
        'new_item'              => 'Новая игра',
        'edit_item'             => 'Редактировать игру',
        'all_items'             => 'Все игры',
        'search_items'          => 'Найти игру',
        'not_found'             => 'Игра не найдена',
        'not_found_in_trash'    => 'Игр нет в корзине',
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-smiley',
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
    );

    register_post_type( 'games' , $args);
}
add_action( 'init', 'games_init' );

/*
 * Add extra fields
 */
function games_extra_fields() {
    add_meta_box(
        'extra_fields',
        __( 'Setting investment company' ),
        'games_fields',
        'games',
        'normal',
        'high'
    );
}
add_action( 'add_meta_boxes', 'games_extra_fields', 1 );

/*
 * Customize default column
 */
function games_custom_column($defaults) {

    $columns = array(
        'cb'	 		=> '<input type="checkbox" />',
        'img'           => '<span class="dashicons dashicons-format-image"></span>',
        'title'			=> 'Название',
        'views'		    => '<span class="dashicons dashicons-visibility"></span> Просмотры',
        'date'			=> 'Дата <span class="dashicons dashicons-calendar"></span>',
    );
    return $columns;
}
add_filter( 'manage_games_posts_columns', 'games_custom_column' );

/*
 * Add sortable fields
 */
function games_sortable( $columns ) {
    //$columns['rate'] = 'rate';
    return $columns;
}
add_filter( "manage_edit-games_sortable_columns", "games_sortable" );

/*
 * Get value to column fields
 */
function games_get_column_value($column, $post_id) {
    if( $column == 'img' )
        echo get_the_post_thumbnail( $post_id, array( 60, 60 ) );

    if( $column == 'views' ) {
        echo getPostViews( $post_id );
    }
}
add_action( 'manage_games_posts_custom_column', 'games_get_column_value', 10, 2 );

/*
 * Extra fields
 */
function games_fields( $post ){
    ?>

    <p>
        <label>Iframe Игры: </label>
        <input style="width:100%;" type="text" placeholder="Ваш Iframe" name="games[iframe]" value="<?= get_post_meta( $post->ID, 'iframe', 1 ) ?>">
    </p>

    <p>
        <label>Ссылка на кнопку "Играть на деньги": </label>
        <input style="width:100%;" type="text" placeholder="http://example.com" name="games[url]" value="<?= get_post_meta( $post->ID, 'url', 1 ) ?>">
    </p>

    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

    <input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

    <?php
}

/*
 * Save investment extra fields
 */
function games_update($post_id ) {
    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;

    if( !isset($_POST['games']) ) return false;

    foreach( $_POST['games'] as $key=>$value ){
        if( empty($value) ){
            delete_post_meta( $post_id, $key );
            continue;
        }

        update_post_meta($post_id, $key, $value);
    }
    return $post_id;
}
add_action( 'save_post', 'games_update', 0 );

/*
 * Desc: Register a post taxonomy.
 * Name Post: games
 */
function create_taxonomy(){
    // заголовки
    $labels = array(
        'name'              => 'Игровые залы',
        'singular_name'     => 'Игровой зал',
        'search_items'      => 'Найти зал',
        'all_items'         => 'Все залы',
        'parent_item'       => 'Подзал',
        'parent_item_colon' => 'Подзал:',
        'edit_item'         => 'Редактировать данные игрового зала',
        'update_item'       => 'Обновить данные игрового зала',
        'add_new_item'      => 'Добавить новый игровой зал',
        'new_item_name'     => 'Новый игровой зал',
        'menu_name'         => 'Игровой зал',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('game-rooms', array('games'), $args );
}
add_action('init', 'create_taxonomy');

// добавляет вызов функции при инициализации административного раздела
add_action('admin_init', 'category_custom_fields', 1);
// функция расширения функционала административного раздела
function category_custom_fields() {
    // добавления действия после отображения формы ввода параметров категории
    add_action('game-rooms_edit_form_fields', 'category_custom_fields_form');
    // add_action('cat_add_form_fields', 'category_custom_fields_form');   - можно добавить поля при создании категории, но могут быть проблемы, так как категории сохраняются через ajax
    // добавления действия при сохранении формы ввода параметров категории
    add_action('edited_game-rooms', 'category_custom_fields_save');
}

function category_custom_fields_form($tag) {
    $t_id = $tag->term_id;
    $cat_meta = get_option("category_$t_id");
    wp_enqueue_media();
    ?>
    <!-- изображение 1 -->
    <tr class="form-field">
        <th scope="row" valign="top"><label for="extra1">Изображение:</label></th>
        <td>
            <p><button class="upload_cat_img">Загрузить</button><br><br><img width="100px" src="<?= $cat_meta['cat_title'] ? $cat_meta['cat_title'] : ''; ?>" /></p>
            <input class="cat_img" type="hidden" name="Cat_meta[cat_title]" id="Cat_meta[cat_title]" size="25" style="width:60%;" value="<?php
            echo
            $cat_meta['cat_title'] ? $cat_meta['cat_title'] : '';
            ?>"><br />
        </td>
    </tr>
    <script>
        jQuery(document).ready(function(){

            jQuery('.upload_cat_img').click(function(e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Upload Image',
                    // mutiple: true if you want to upload multiple files at once
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first();
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Output to the console uploaded_image
                        console.log(uploaded_image);
                        var image_url = uploaded_image.toJSON().url;
                        // Let's assign the url value to the input field
                        jQuery('.cat_img').val(image_url);
                    });
            });
        });
    </script>
    <?php
}

function category_custom_fields_save($term_id) {
    if (isset($_POST['Cat_meta'])) {
        $t_id = $term_id;
        $cat_meta = get_option("category_$t_id");
        $cat_keys = array_keys($_POST['Cat_meta']);
        foreach ($cat_keys as $key) {
            if (isset($_POST['Cat_meta'][$key])) {
                $cat_meta[$key] = $_POST['Cat_meta'][$key];
            }
        }
//save the option array
        update_option("category_$t_id", $cat_meta);
    }
}